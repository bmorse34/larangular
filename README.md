# Laravel and Angular Example

- Change your database settings in `app/config/database.php`
- Migrate your database: `php artisan migrate`
- Seed your database: `php artisan db:seed`

Just trying out different angular features, below are URLs to adding a comment, adding a breed, adding a photo (without the actual photo upload) and a photo upload using a angular photo upload plugin

- larangular.dev/
- larangular.dev/breed
- larangular.dev/photo
- larangular.dev/upload (set user_images within public to chmod 777)