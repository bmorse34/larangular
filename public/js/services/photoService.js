angular.module('photoService', [])

	.factory('Photo', function($http) {

		return {
			get : function() {
				return $http.get('photo_api/photos');
			},
			show : function(id) {
				return $http.get('photo_api/photos/' + id);
			},
			save : function(photoData) {
				return $http({
					method: 'POST',
					url: 'photo_api/photos',
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(photoData)
				});
			},
			destroy : function(id) {
				return $http.delete('photo_api/photos/' + id);
			},
			get_breeds_list : function() {
				return $http.get('/get_breeds_list');
			},
		}

	});