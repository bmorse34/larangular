angular.module('breedService', [])

	.factory('Breed', function($http) {

		return {
			get : function() {
				return $http.get('breed_api/breeds');
			},
			show : function(id) {
				return $http.get('breed_api/breeds/' + id);
			},
			save : function(breedData) {
				return $http({
					method: 'POST',
					url: 'breed_api/breeds',
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(breedData)
				});
			},
			destroy : function(id) {
				return $http.delete('breed_api/breeds/' + id);
			},
			get_breeds_list : function() {
				return $http.get('breed/select_breeds');
			},
			save_file : function(breedData) {
				return $http({
					method: 'POST',
					url: 'breed/save_file',
					headers: { 'Content-Type' : 'multipart/form-data' },
					data: $.param(breedData)
				});
			},
		}

	});