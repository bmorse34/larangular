angular.module('photoCtrl', [])

	.controller('photoController', function($scope, $http, Photo) {
		// object to hold all the data for the new comment form
		$scope.photoData = {};

		// loading variable to show the spinning loading icon
		$scope.loading = true;
		
		// get all the comments first and bind it to the $scope.photos object
		Photo.get()
			.success(function(data) {
				$scope.photos = data;
				$scope.loading = false;
			});

		Photo.get_breeds_list()
			.success(function(data) {
				$scope.breeds_list = data;
				$scope.breed_id = null;
				// $scope.loading = false;
			});


		// function to handle submitting the form
		$scope.submitPhoto = function() {
			$scope.loading = true;

			// save the comment. pass in comment data from the form
			Photo.save($scope.photoData)
				.success(function(data) {

					// if successful, we'll need to refresh the comment list
					Photo.get()
						.success(function(getData) {
							$scope.photos = getData;
							$scope.loading = false;
						});

				})
				.error(function(data) {
					console.log(data);
				});
		};

		// function to handle deleting a comment
		$scope.deletePhoto = function(id) {
			$scope.loading = true; 

			Photo.destroy(id)
				.success(function(data) {

					// if successful, we'll need to refresh the comment list
					Photo.get()
						.success(function(getData) {
							$scope.photos = getData;
							$scope.loading = false;
						});

				});
		};
	});