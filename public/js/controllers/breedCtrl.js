angular.module('breedCtrl', [])

	.controller('breedController', function($scope, $http, Breed) {
		// object to hold all the data for the new comment form
		$scope.breedData = {};

		// loading variable to show the spinning loading icon
		$scope.loading = true;
		
		// get all the comments first and bind it to the $scope.breeds object
		Breed.get()
			.success(function(data) {
				$scope.breeds = data;
				$scope.loading = false;
			});

		Breed.get_breeds_list()
			.success(function(data) {
				$scope.breeds_list = data;
				$scope.breed_id = null;
				// $scope.loading = false;
			});


		// function to handle submitting the form
		$scope.submitBreed = function() {
			$scope.loading = true;

			// save the comment. pass in comment data from the form
			Breed.save($scope.breedData)
				.success(function(data) {

					// if successful, we'll need to refresh the comment list
					Breed.get()
						.success(function(getData) {
							$scope.breeds = getData;
							$scope.loading = false;
						});

				})
				.error(function(data) {
					console.log(data);
				});
		};

		// function to handle deleting a comment
		$scope.deleteBreed = function(id) {
			$scope.loading = true; 

			Breed.destroy(id)
				.success(function(data) {

					// if successful, we'll need to refresh the comment list
					Breed.get()
						.success(function(getData) {
							$scope.breeds = getData;
							$scope.loading = false;
						});

				});
		};

		// function to handle submitting the form
		$scope.submitFile = function() {
	        var uploader = $scope.uploader = new FileUploader({
	            url: 'breed/save_file'
	        });

	        // FILTERS

	        uploader.filters.push({
	            name: 'customFilter',
	            fn: function(item /*{File|FileLikeObject}*/, options) {
	                return this.queue.length < 10;
	            }
	        });			

	        console.info('uploader', uploader);

		};
	});