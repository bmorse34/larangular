<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('photos', function($table){
	        $table->increments('id');
	        $table->enum('status', array('pending', 'approved', 'rejected'))->default('pending');
	        $table->string('caption', 140);
	        $table->string('filename', 64);
	        $table->enum('source', array('direct', 'facebook'))->nullable();
	        $table->string('source_id', 100)->nullable();
	        $table->integer('breed_id');
	        $table->timestamps();
	        // $table->dateTime('created_at');

	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('photos');
	}

}
