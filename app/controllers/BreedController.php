<?php

class BreedController extends \BaseController {

	/**
	 * Send back all comments as JSON
	 *
	 * @return Response
	 */
	public function index()
	{
		return Response::json(Breed::get());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		Breed::create(array(
			'name' => Input::get('name'),
			'description' => Input::get('description')
		));

		return Response::json(array('success' => true));
	}

	/**
	 * Return the specified resource using JSON
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Response::json(Breed::find($id));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Breed::destroy($id);

		return Response::json(array('success' => true));
	}


	public function select_breeds()
	{
		return Response::json(Breed::get());
	}

	public function save_file()
	{
		$file = Input::file('file');
		$file_extension = $file->getClientOriginalExtension();
		$destinationPath = public_path().'/user_images/';
		$filename = str_random(18).'.'.$file_extension;
		$upload_success = Input::file('file')->move($destinationPath, $filename);

		if($upload_success)
		{
			return Response::json(array('success' => true));
		} else {
			return Response::json(array('success' => false));
		}
	}
}