<?php

class PhotoController extends \BaseController {

	/**
	 * Send back all comments as JSON
	 *
	 * @return Response
	 */
	public function index()
	{
		return Response::json(Photo::get_photos_with_breed_name());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		Photo::create(array(
			'caption' => Input::get('caption'),
			'filename' => 'filler.jpg',
			'breed_id' => Input::get('breed_id')
		));

		return Response::json(array('success' => true));
	}

	/**
	 * Return the specified resource using JSON
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Response::json(Photo::find($id));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Photo::destroy($id);

		return Response::json(array('success' => true));
	}


	public function select_breeds()
	{
		return Response::json(Breed::get());
	}

	public function save_file()
	{
		$file = Input::file('file');
		$file_extension = $file->getClientOriginalExtension();
		$destinationPath = public_path().'/user_images/';
		$filename = str_random(18).'.'.$file_extension;
		$upload_success = Input::file('file')->move($destinationPath, $filename);

		if($upload_success)
		{
			return Response::json(array('success' => true));
		} else {
			return Response::json(array('success' => false));
		}
	}
}