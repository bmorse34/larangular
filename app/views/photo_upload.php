<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Testing Angular with Laravel</title>

	<!-- CSS -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css"> <!-- load bootstrap via cdn -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"> <!-- load fontawesome -->
	<style>
		body 		{ padding-top:30px; }
		form 		{ padding-bottom:20px; }
		.comment 	{ padding-bottom:20px; }
	</style>

	<!-- JS -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.8/angular.min.js"></script> <!-- load angular -->

	<!-- ANGULAR -->
	<!-- all angular resources will be loaded from the /public folder -->
	<script src="js/controllers/photoCtrl.js"></script> <!-- load our controller -->
	<script src="js/services/photoService.js"></script> <!-- load our service -->
	<script src="js/app.js"></script> <!-- load our application -->

</head>
<!-- declare our angular app and controller -->
<body class="container" ng-app="photoApp" ng-controller="photoController">
<div class="col-md-8 col-md-offset-2">

	<!-- PAGE TITLE -->
	<div class="page-header">
		<h2>Add Caption and Select type of dog breed</h2>
	</div>

	<!-- NEW BREED FORM -->
	<form ng-submit="submitPhoto()"> <!-- ng-submit will disable the default form action and use our function -->

		<!-- BREED NAME -->
		<div class="form-group">
			<textarea type="text" class="form-control input-sm" name="caption" ng-model="photoData.caption" placeholder="Caption"></textarea>
		</div>

		<div class="form-group">
			<select ng-options="breed.id as breed.name for breed in breeds_list" ng-model="photoData.breed_id" name="breed_id" required></select>
		</div>
		
		<!-- SUBMIT BUTTON -->
		<div class="form-group text-right">	
			<button type="submit" class="btn btn-primary btn-lg">Submit</button>
		</div>
	</form>

	<pre>
	{{ photoData }}
	</pre>

	<!-- LOADING ICON -->
	<!-- show loading icon if the loading variable is set to true -->
	<p class="text-center" ng-show="loading"><span class="fa fa-meh-o fa-5x fa-spin"></span></p>

	<!-- THE COMMENTS -->
	<!-- hide these comments if the loading variable is true -->
	<div class="comment" ng-hide="loading" ng-repeat="photo in photos">
		<p>Caption: {{ photo.caption }}</p>
		<p>Breed: {{ photo.breed_name }}</p>

		<p><a href="#" ng-click="deletePhoto(photo.id)" class="text-muted">Delete</a></p>
	</div>

</div>
</body>
</html>