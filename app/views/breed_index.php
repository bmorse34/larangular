<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Laravel and Angular Comment System</title>

	<!-- CSS -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css"> <!-- load bootstrap via cdn -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"> <!-- load fontawesome -->
	<style>
		body 		{ padding-top:30px; }
		form 		{ padding-bottom:20px; }
		.comment 	{ padding-bottom:20px; }
	</style>

	<!-- JS -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.8/angular.min.js"></script> <!-- load angular -->

	<!-- ANGULAR -->
	<!-- all angular resources will be loaded from the /public folder -->
	<script src="js/controllers/breedCtrl.js"></script> <!-- load our controller -->
	<script src="js/services/breedService.js"></script> <!-- load our service -->
	<script src="js/app.js"></script> <!-- load our application -->
	<script src="js/angular-file-upload.js"></script> <!-- load our application -->
	<script src="js/controllers.js"></script> <!-- load our application -->

</head>
<!-- declare our angular app and controller -->
<body class="container" ng-app="breedApp" ng-controller="breedController">
<div class="col-md-8 col-md-offset-2">

	<!-- PAGE TITLE -->
	<div class="page-header">
		<h2>Laravel and Angular Single Page Application</h2>
		<h4>Commenting System</h4>
	</div>

	<!-- NEW BREED FORM -->
	<form ng-submit="submitFile()"> <!-- ng-submit will disable the default form action and use our function -->

		<!-- BREED NAME -->
		<div class="form-group">
			<input type="text" class="form-control input-sm" name="name" ng-model="breedData.name" placeholder="Name">
		</div>
		<div class="form-group">
			<input type="text" class="form-control input-sm" name="description" ng-model="breedData.description" placeholder="Description">
		</div>

		<div class="form-group">
			Single
			<input type="file" nv-file-select="" uploader="uploader" />
		</div>


<!-- 		<div class="comment" ng-hide="loading" ng-repeat="breed in breeds_list">
			<p>Howdy {{ breed }}</p>
		</div>
 -->
		<div class="form-group">
			<select ng-options="breed.id as breed.name for breed in breeds_list" ng-model="breedData.breed_id" required></select>

		</div>
		
		<!-- SUBMIT BUTTON -->
		<div class="form-group text-right">	
			<button type="submit" class="btn btn-primary btn-lg">Submit</button>
		</div>
	</form>

	<pre>
	{{ breedData }}
	</pre>

	<!-- LOADING ICON -->
	<!-- show loading icon if the loading variable is set to true -->
	<p class="text-center" ng-show="loading"><span class="fa fa-meh-o fa-5x fa-spin"></span></p>

	<!-- THE COMMENTS -->
	<!-- hide these comments if the loading variable is true -->
	<div class="comment" ng-hide="loading" ng-repeat="breed in breeds">
		<p>{{ breed.name }}</p>

		<p><a href="#" ng-click="deleteBreed(breed.id)" class="text-muted">Delete</a></p>
	</div>

</div>
</body>
</html>