<?php

class Photo extends Eloquent {
	protected $fillable = array('caption', 'filename', 'breed_id');	

    public function breed()
    { 
    	return $this->belongsTo('Breed'); 
    }

    public static function get_photos_with_breed_name()
    {
    	return DB::table('photos')
        ->leftJoin('breeds', 'photos.breed_id', '=', 'breeds.id')
        ->select('photos.*', 'breeds.name as breed_name')
        ->get();
    }
}