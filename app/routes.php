<?php

// =============================================
// HOME PAGE ===================================
// =============================================
Route::get('/', function()
{
	return View::make('index');
});

Route::group(array('prefix' => 'photo_api'), function() {
	Route::resource('photos', 'PhotoController', 
		array('except' => array('create', 'edit', 'update')));
});

Route::get('/photos', function()
{
	return View::make('photo_upload');
});

Route::get('/upload', function()
{
	return View::make('upload');
});

Route::post('/upload', function()
{
	return Response::json(array('success' => true));
});

Route::get('/breeds', function()
{
	return View::make('breed_index');
});

Route::get('/get_breeds_list', function()
{
	return Response::json(Breed::get());
});

Route::get('breed/select_breeds', 'BreedController@select_breeds');
Route::get('photo/select_breeds', 'BreedController@select_breeds');
Route::post('breed/save_file', 'BreedController@save_file');


// =============================================
// API ROUTES ==================================
// =============================================
Route::group(array('prefix' => 'api'), function() {
	Route::resource('comments', 'CommentController', 
		array('except' => array('create', 'edit', 'update')));
});

// =============================================
// CATCH ALL ROUTE =============================
// =============================================
App::missing(function($exception)
{
	return View::make('index');
});

// =============================================
// API ROUTES ==================================
// =============================================
Route::group(array('prefix' => 'breed_api'), function() {
	Route::resource('breeds', 'BreedController', 
		array('except' => array('create', 'edit', 'update')));
});